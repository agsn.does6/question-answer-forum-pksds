{{-- <!DOCTYPE html>
<html>
<head>
    <title>Question Details</title>
</head>
<body>
    <h1>Question Details</h1>
    <h2>{{ $question['title'] }}</h2>
    <p>{{ $question['content'] }}</p>

    <h3>Answers</h3>
    <ul>
        @foreach ($answers as $answer)
        <li>
            {{ $answer['content'] }}
        </li>
        @endforeach
    </ul>

    <h3>Create Answer</h3>
    <form action="{{ route('answers.store', $question['id']) }}" method="POST">
        @csrf
        <textarea name="content"></textarea>
        <button type="submit">Submit Answer</button>
    </form>
</body>
</html> --}}
{{-- <!DOCTYPE html>
<html>
<head>
    <title>Question Details</title>
</head>
<body>
    <h1>Question Details</h1>
    <h2>{{ $question->title }}</h2>
    <p>{{ $question->content }}</p>

    <h3>Answers</h3>
    <ul>
        @foreach ($answers as $answer)
        <li>
            {{ $answer->content }}
        </li>
        @endforeach
    </ul>

    <h3>Create Answer</h3>
    <form action="{{ route('answers.store', $question->id) }}" method="POST">
        @csrf
        <textarea name="content"></textarea>
        <button type="submit">Submit Answer</button>
    </form>
</body>
</html> --}}
@extends('layout.master')

@section('title')
    Halaman Detail Pertanyaan
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <h5 class="card-title"><b>{{$question->title}}</b></h5><br/>
    <span class="badge badge-pill badge-dark mb-3">Kategori: {{$question->category->name}}</span>
    <img src="{{ asset('images/' . $question['image']) }}" class="card-img-top" />
    <p class="card-text">{{ $question->content }}</p>

    <hr/>
    <h3>Jawaban</h3>
    @forelse ($question->answers as $item)
    <div class="media my-3 border p-3">
        <img src="https://dummyimage.com/300" class="mr-3" alt="..." style="border-radius: 50%" width="100px">
        <div class="media-body">
          <h5 class="mt-0">{{$item->user->name}}</h5>
          <p>{{$item->content}}</p>
        </div>
      </div>
    @empty
    <span> </span>
    @endforelse
    <form action="/questions/{{$question->id}}" method="POST">
        @csrf
        <textarea name="content" cols="30" rows="10" class="form-control my-3" placeholder="Isi jawaban Anda disini"></textarea>
        <input type="submit" value="Jawab" class="mb-3">
    </form>
    
    <a href="/questions" class="btn btn-sm btn-info btn-block">Kembali</a>
  </div>
</div>
@endsection

