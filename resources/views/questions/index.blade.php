{{-- @extends('layout.master')

@section('title')
    Beranda
@endsection

@section('content')
    <h1>Questions</h1>
    <ul>
        @foreach($questions as $question)
            <li>
                <a href="{{ route('questions.show', $question['id']) }}">{{ $question['title'] }}</a>
            </li>
        @endforeach
    </ul>
@endsection
 --}}
 @extends('layout.master')

@section('title')
    Halaman Questions
@endsection

@section('content')

  <a href="/questions/buat" class="btn btn-primary mb-2 ml-2" tabindex="-1" role="button" aria-disabled="true">Ask Question</a>
  <hr/>
  <section>
    <div class="row">
      @forelse ($questions as $item)
      <div class="mx-3 my-2" >
        <div class="card" style="min-heightht: 6rem; width: 18rem; background-color: gainsboro">
          <div class="card-body">
            <h5 class="card-title"><b>{{$item['title']}}</b></h5><br/>
            <span class="badge badge-pill badge-dark mb-3">Kategori: {{$item->category['name']}}</span>
            @if ($item->image)
              <img src="{{ asset('images/' . $item['image']) }}" class="img-thumbnail" style="width: 300px; height: 300px;"/>
            @else
              &nbsp;
            @endif
            <p class="card-text">{{ Str::limit($item['content'], 100) }}</p>
            {{-- <p class="card-text">{{ Str::limit($item->bio, 100) }}</p> --}}
            
            <a href="/questions/{{$item['id']}}" class="btn btn-sm btn-info">Beri jawaban</a>
            <a href="/questions/{{$item['id']}}/edit" class="btn btn-sm btn-warning">Update</a>
            <form action="/questions/{{$item['id']}}" method="POST" style="display: inline">
              @csrf
              @method('delete')
              <button type="submit" class="btn btn-sm btn-danger">Delete</a>
            </form>
          </div>
        </div>
      </div>
      @empty
        <div class="ml-3" style="display: inline-block">
          <h2> Belum ada data</h2>
          <p> Klik 'Ask Question' untuk menambahkan pertanyaan Anda</p>
        </div>
      @endforelse
    </div>
  </section>
@endsection
   

