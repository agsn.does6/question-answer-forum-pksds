<div class="sidebar">
  <!-- Sidebar user (optional) -->
  @auth
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="{{asset('/template/dist/img/user-default.avif')}}" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="/profile" class="d-block">{{ Auth::user()->name }} - 
        @if(Auth::user()->age)
        ({{ Auth::user()->age }})
        @else
            (0)
        @endif
      </a>
    </div>
  </div>
  @endauth
  @guest
    <p></p>
  @endguest

  <!-- SidebarSearch Form -->
  <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      
      <li class="nav-item">
        <a href="/" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Public
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="/questions" class="nav-link">
          <i class="nav-icon fas fa-question"></i>
          <p>
            Question
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="/categories" class="nav-link">
          <i class="nav-icon fas fa-filter"></i>
          <p>
            Category
          </p>
        </a>
      </li>
      @guest
      <li class="nav-item bg-info">
        <a href="/login" class="nav-link" onclick="toggleContainer(event)">
          <p>
            Login
          </p>
        </a>
      </li>
      @endguest
      
      @auth
      <li class="nav-item bg-danger" aria-labelledby="navbarDropdown">
        {{-- <a class="nav-link" href="/login" --}}
        <a class="nav-link" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        {{-- <form id="logout-form" action="/login" method="POST" class="d-none"> --}}
            @csrf
        </form>
      </li>
      @endauth
  
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<script>
  function toggleContainer(event) {
    event.preventDefault();
    var container = document.querySelector('.container');
    container.style.display = 'block';
  }
</script>