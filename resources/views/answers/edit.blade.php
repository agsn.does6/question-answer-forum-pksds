<!DOCTYPE html>
<html>
<head>
    <title>Edit Answer</title>
</head>
<body>
    <h1>Edit Answer</h1>
    <form action="{{ route('answers.update', [$questionId, $answerId]) }}" method="POST">
        @csrf
        @method('PUT')
        <textarea name="content">{{ $answer['content'] }}</textarea>
        <button type="submit">Update Answer</button>
    </form>
</body>
</html>
