@extends('layout.master')

@section('title')
    Form Create Category
@endsection

@section('content')
  <section>
    <form action="/categories" method="POST">
      @csrf
      <div class="form-floating mb-3">
        <label for="floatingInputNama">Nama Kategori</label>
        <input type="text" class="form-control" id="floatingInputNama" placeholder="Masukkan nama kategori baru" name="name">
      </div>
      @error('name')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <button type="submit" class="btn btn-danger">Submit</button>
    </form>
  </section>
@endsection
