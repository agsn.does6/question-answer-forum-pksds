<!DOCTYPE html>
<html>
<head>
    <title>Category Details</title>
</head>
<body>
    <h1>Category Details</h1>
    <p>Name: {{ $category['name'] }}</p>
    <p>Description: {{ $category['description'] }}</p>
</body>
</html>
