@extends('layout.master')

@section('title')
    Category
@endsection

@section('content')
    <a href="/categories/create" class="btn btn-primary mb-2 ml-2" tabindex="-1" role="button" aria-disabled="true">Tambah Kategori Baru</a>
    <hr/>
  <table class="table">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Kategori</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($categories as $category)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $category['name'] }}</td>
            <td>
                <a href="/categories/{{ $category['id'] }}/edit" class="btn btn-sm btn-warning">Update</a>
                <form action="/categories/{{ $category['id'] }}" method="POST" style="display: inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="3">
                <div class="text-center flex justify-center my-3">
                    <h2>Belum ada data</h2>
                    <p>Klik 'Tambah Kategori Baru' untuk menambahkan data kategori</p>
                </div>
            </td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection
