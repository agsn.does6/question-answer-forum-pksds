@extends('layout.master')

@section('title')
    Form Update Category
@endsection

@section('content')
  <section>
    <form action="/categories/{{$category['id']}}" method="POST">
      @csrf
      @method('put')
      <div class="form-floating mb-3">
        <label for="floatingInput">Nama Kategori</label>
        <input type="text" class="form-control" id="floatingInput" placeholder="Masukkan nama kategori" nama lengkap" name="name" value="{{$category['name']}}">
      </div>
      @error('name')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <button type="submit" class="btn btn-danger">Update</button>
    </form>
  </section>
@endsection
