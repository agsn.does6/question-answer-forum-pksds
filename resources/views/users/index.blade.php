{{-- <!DOCTYPE html>
<html>
<head>
    <title>Users</title>
</head>
<body>
    <h1>Users</h1>
    <ul>
        @foreach($users as $user)
            <li>
                <a href="{{ route('users.show', $user['id']) }}">{{ $user['name'] }}</a>
            </li>
        @endforeach
    </ul>
</body>
</html> --}}
@extends('layout.master')
@section('title')
    Profile
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-warning card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src={{asset('/template/dist/img/user-default.avif')}}
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">Nina Mcintire</h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nama</b> <a class="float-right text-dark">{{ $user->name }}</a>
                </li>
                <li class="list-group-item">
                  <b>Email</b> <a class="float-right text-dark">{{ $user->email }}</a>
                </li>
                <li class="list-group-item">
                  <b>Umur</b> <a class="float-right text-dark">{{ $user->age }} Tahun</a>
                </li>
              </ul>

              {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">About Me</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

              <p class="text-muted">
                {{ $user->address }}
              </p>

              <strong><i class="fas fa-map-book mr-1"></i> Biografi</strong>

              <p class="text-muted">{{ $user->bio }}</p>
              <hr>


              <hr>

              {{-- <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

              <p class="text-muted">
                <span class="tag tag-danger">UI Design</span>
                <span class="tag tag-success">Coding</span>
                <span class="tag tag-info">Javascript</span>
                <span class="tag tag-warning">PHP</span>
                <span class="tag tag-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

              <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p> --}}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active bg-warning" href="#settings" data-toggle="tab">Form Update Profile</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
                <div class="active tab-pane" id="settings">
                  <form class="form-horizontal" action="{{ route('users.update') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputName" placeholder="Masukkan nama Anda" name="name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <input disabled type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{$user->email}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputSkills" class="col-sm-2 col-form-label">Umur</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputAge" placeholder="Masukkan umur Anda disini" name="age">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="inputBio" style="height: 70px" placeholder="Lengkapi alamat tinggal Anda" name="address"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Biografi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputBio" style="height: 100px" placeholder="Tuliskan tentang Anda disini" name="bio"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="offset-sm-2 col-sm-10">
                        <button type="submit" class="btn btn-danger">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  @endsection
