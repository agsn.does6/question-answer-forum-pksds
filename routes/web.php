<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');

// form registrasi
Route::get('/register', [AuthController::class, 'showRegistrationForm'])->name('register');
// registrasi
Route::post('/register', [AuthController::class, 'register'])->name('register.post');

// form login
Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login');
// login
Route::post('/login', [AuthController::class, 'login'])->name('login.post');

// logout
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

// Home Page







Route::get('/master', function () {
  return view('layout.master');
});

Route::middleware(['auth'])->group(function () {
  // CategoryController
  Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
  Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
  Route::get('/categories/{id}', [CategoryController::class, 'show'])->name('categories.show');
  Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
  Route::get('/categories/{id}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
  Route::put('/categories/{id}', [CategoryController::class, 'update'])->name('categories.update');
  Route::delete('/categories/{id}', [CategoryController::class, 'destroy'])->name('categories.destroy');

  // QuestionController
  Route::get('/questions', [QuestionController::class, 'index'])->name('questions.index');
  // Route::get('/questions/create', [QuestionController::class, 'create'])->name('questions.create');
  Route::get('/questions/buat', [QuestionController::class, 'create'])->name('questions.create');
  Route::get('/questions/{id}', [QuestionController::class, 'show'])->name('questions.show');
  Route::post('/questions', [QuestionController::class, 'store'])->name('questions.store');
  Route::get('/questions/{id}/edit', [QuestionController::class, 'edit'])->name('questions.edit');
  Route::put('/questions/{id}', [QuestionController::class, 'update'])->name('questions.update');
  Route::delete('/questions/{id}', [QuestionController::class, 'destroy'])->name('questions.destroy');

  // AnswerController
Route::post('/questions/{questionId}', [AnswerController::class, 'store'])->name('answers.store');
Route::get('/questions/{questionId}/answers/{answerId}/edit', [AnswerController::class, 'edit'])->name('answers.edit');
Route::put('/questions/{questionId}/answers/{answerId}', [AnswerController::class, 'update'])->name('answers.update');
Route::delete('/questions/{questionId}/answers/{answerId}', [AnswerController::class, 'destroy'])->name('answers.destroy');

  // UserController
  Route::get('/profile', [UserController::class, 'index'])->name('users.index');
  Route::put('/profile', [UserController::class, 'update'])->name('users.update');
});

Auth::routes();
