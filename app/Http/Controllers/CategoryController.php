<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); // Menambahkan middleware 'auth' untuk memastikan pengguna telah login
    }

    public function index()
    {
        $categories = Category::all();

        return view('categories.index', compact('categories'));
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);

        return view('categories.show', compact('category'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();

        Alert::success('Sukses', 'Data kategori berhasil ditambahkan!')->persistent(true)->autoClose(3000);
        return redirect('/categories');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $category = Category::findOrFail($id);

        // Memastikan pengguna hanya dapat mengubah kategori miliknya sendiri
        // if ($category->user_id === Auth::user()->id) {
            $category->name = $request->name;
            $category->save();

            Alert::success('Terupdate', 'Data kategori berhasil diupdate!')->persistent(true)->autoClose(3000);
            return redirect('/categories');
        // } else {
            // Jika pengguna mencoba mengubah kategori milik orang lain, arahkan mereka ke halaman yang sesuai
            // return redirect('/categories')->with('error', 'Anda tidak memiliki izin untuk mengubah kategori ini.');
        // }
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        // Memastikan pengguna hanya dapat menghapus kategori miliknya sendiri
        // if ($category->user_id === Auth::user()->id) {
            $category->delete();

            Alert::success('Terhapus', 'Data kategori berhasil dihapus!')->persistent(true)->autoClose(3000);
            return redirect('/categories')->with('success', 'Category deleted successfully.');
        // } else {
            // Jika pengguna mencoba menghapus kategori milik orang lain, arahkan mereka ke halaman yang sesuai
            // return redirect('/categories')->with('error', 'Anda tidak memiliki izin untuk menghapus kategori ini.');
        // }
    }
}
