<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $categories
        ]);
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);

        if (!$category) {
            return response()->json([
                'code' => 404,
                'message' => 'Category not found'
            ], 404);
        }

        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $category
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'user_id' => 'required|exists:users,id',
        ]);

        $category = Category::create($request->all());
        return response()->json([
            'code'  => 201,
            'message' => 'Created',
            'data' => $category
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'user_id' => 'required|exists:users,id',
        ]);

        $category = Category::findOrFail($id);

        if (!$category) {
            return response()->json([
                'code' => 404,
                'message' => 'Category not found'
            ], 404);
        }

        $category->update($request->all());
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $category
        ]);
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        if (!$category) {
            return response()->json([
                'code' => 404,
                'message' => 'Category not found'
            ], 404);
        }

        $category->delete();
        return response()->json([
            'code' => 200,
            'message' => 'Category deleted successfully'
        ]);
    }
}
