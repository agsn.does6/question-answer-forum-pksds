<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        return response()->json([
            'code' => 200,
            'message' => "OK",
            'data' => $questions
        ]);
    }

    public function show($id)
    {
        $question = Question::findOrFail($id);

        if (!$question) {
            return response()->json([
                'code' => 404,
                'message' => 'Question not found'
            ], 404);
        }

        return response()->json([
            'code' => 200,
            'message' => "OK",
            'data' => $question
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string',
            'content' => 'required|string',
        ]);

        $question = Question::create($request->all());
        return response()->json([
            'code' => 201,
            'message' => 'Created',
            'data' => $question
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string',
            'content' => 'required|string',
        ]);

        $question = Question::findOrFail($id);

        if (!$question) {
            return response()->json([
                'code' => 404,
                'message' => 'Question not found'
            ], 404);
        }

        $question->update($request->all());
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $question
        ]);
    }

    public function destroy($id)
    {
        $question = Question::findOrFail($id);

        if (!$question) {
            return response()->json([
                'code' => 404,
                'message' => 'Question not found'
            ], 404);
        }

        $question->delete();
        return response()->json([
            'code' => 200,
            'message' => 'Question deleted successfully'
        ]);
    }
}
