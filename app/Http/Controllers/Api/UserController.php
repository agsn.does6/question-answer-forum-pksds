<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $users
        ]);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        if (!$user) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found'
            ], 404);
        }

        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $user
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'age' => 'required|integer',
            'address' => 'required|string',
        ]);

        $user = User::create($request->all());
        return response()->json([
            'code' => 201,
            'message' => 'Created',
            'data' => $user
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'age' => 'required|integer',
            'address' => 'required|string',
        ]);

        $user = User::findOrFail($id);

        if (!$user) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found'
            ], 404);
        }

        $user->update($request->all());
        return response()->json([
            'code' => 200,
            'message' => 'OK',
            'data' => $user
        ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if (!$user) {
            return response()->json([
                'code' => 404,
                'message' => 'User not found'
            ], 404);
        }

        $user->delete();
        return response()->json([
            'code' => 200,
            'message' => 'User deleted successfully'
        ]);
    }
}
